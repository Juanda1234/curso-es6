const URL_PATH = 'https://api.themoviedb.org';
const API_KEY = '9f256582b4040e6fc78b7a6ca742ccb7';
const NOW_PLAYING_MOVIE = 'now_playing';
const POPULAR_MOVIE = 'popular';
const TOP_RATED_MOVIE = 'top_rated';
const LIST_TOP_RATED_MOVIE = 'top-rated-playing__list';
const LIST_POPULAR_MOVIE = 'now-playing__list';


document.addEventListener("DOMContentLoaded", () => {
    renderNewsMovies();
    renderTopPopularMovie(LIST_TOP_RATED_MOVIE, TOP_RATED_MOVIE);
    renderTopPopularMovie(LIST_POPULAR_MOVIE, POPULAR_MOVIE);
});

const getMovies = (type) => {
    const url = `${URL_PATH}/3/movie/${type}?api_key=${API_KEY}&language=es-ES&page=1`;

    return fetch(url)
        .then(response => response.json())
        .then(result => result.results)
        .catch(error => console.log(error));
}

const renderNewsMovies = async () => {
    const newMovies = await getMovies(NOW_PLAYING_MOVIE);
    console.log(newMovies);

    let html = '';

    newMovies.forEach((movie, index) => {
        const {id, title, overview, backdrop_path} = movie;
        const urlImage = `https://image.tmdb.org/t/p/original${backdrop_path}`;
        const urlMovie = `../movie.html?id=${id}`;
        html += `
            <div class="carousel-item ${index == 0 ? 'active' : null}" style="background-image: url('${urlImage}')">
                <div class="carousel-caption">
                    <h5>${title}</h5>
                    <p>${overview}</p>
                    <a href="${urlMovie}" class="btn btn-primary">Más información</a>
                </div>
            </div>
        `;
    });

    html += `
        <a class="carousel-control-prev" href="#carousel-news-movies" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-news-movies" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    `;

    document.getElementsByClassName('list-news-movies')[0].innerHTML = html;
}

const renderTopPopularMovie = async (typeList, typeMovieTopRated) => {
    const movies = await getMovies(typeMovieTopRated);

    let html = "";

    movies.forEach((movie, index) => {
        const { id, title, poster_path } = movie;
        const movieCover = `https://image.tmdb.org/t/p/w500${poster_path}`;
        const urlMovie = `../movie.html?id=${id}`;

        if(index < 5){
            html += `
                <li class="list-group-item">
                    <img src="${movieCover}" alt="${title}">
                    <h3>${title}</h3>
                    <a href="${urlMovie}" class="btn btn-primary">Ver más</a>
                </li>
            `;
        }
    });

    document.getElementsByClassName(typeList)[0].innerHTML = html;
}

