const URL_PATH = 'https://api.themoviedb.org';
const API_KEY = '9f256582b4040e6fc78b7a6ca742ccb7';

document.addEventListener('DOMContentLoaded', () => {
    let { page } = getUrlVars();
    page == undefined ? page = 1 : null;
    renderNewMovies(page);
    renderControls(page);
    console.log(page);
});


const getUrlVars = () => {
    let vars = {};

    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });

    return vars;
}

const getNewsMovies = (page) => {
    const url = `${URL_PATH}/3/movie/now_playing?api_key=${API_KEY}&language=es-ES&page=${page}`;

    return fetch(url)
        .then(response => response.json())
        .then(result => result.results)
        .catch(error => console.log(error));

}

const renderNewMovies = async (page) => {
    const movies = await getNewsMovies(page);
    console.log('respuesta de la promesa', movies);

    let html = "";

    movies.forEach(movie => {
        const { id, poster_path, title, overview} = movie;
        const urlMovie = `../movie.html?id=${id}`;
        const movieCover = `https://image.tmdb.org/t/p/w500${poster_path}`;
        html += `
            
                <div class="col-xs-12 col-sm-6 col-md-3 col-custom"">
                    <a href="${urlMovie}" class="card custom-card">
                        <img class="card-img-top" src="${movieCover}" alt="${title}">
                        <div class="card-body">
                            <h4 class="card-title text-cente m-0">${title}</h4>
                        </div>
                    </a>
                </div>
            
        `;
    });

    document.getElementsByClassName('list-card')[0].innerHTML = html;
}

const renderControls = (page) => {
    const baseUrlPage = "../now-playing.html?page=";
    const pageNumber = parseInt(page);

    let html = "";

    if(page == 1){
        html = `
            <ul class="pagination justify-content-center">
                <li class="page-item disabled">
                    <a class="page-link" href="#">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                </li>
                <li class="page-item active">
                    <a class="page-link" href="${baseUrlPage + "1"}">1</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="${baseUrlPage + "2"}">2</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="${baseUrlPage + "3"}">3</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="${baseUrlPage + "2"}">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        `;
    }else {

        html = `
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="${baseUrlPage + (pageNumber - 1)}">
                    <i class="fas fa-chevron-left"></i>
                </a>
            </li>
            <li class="page-item">
                <a class="page-link" href="${baseUrlPage + (pageNumber - 1)}">${pageNumber - 1}</a>
            </li>
            <li class="page-item ${page == page ? 'active' : null}">
                <a class="page-link" href="${baseUrlPage + pageNumber}">${pageNumber}</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="${baseUrlPage + (pageNumber + 1)}">${pageNumber + 1}</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="${baseUrlPage + (pageNumber + 1)}">
                    <i class="fas fa-chevron-right"></i>
                </a>
            </li>
        </ul>
    `;
    }

    document.getElementsByClassName('navigation')[0].innerHTML = html;

}