const URL_PATH = 'https://api.themoviedb.org';
const API_KEY = '9f256582b4040e6fc78b7a6ca742ccb7';

document.addEventListener('DOMContentLoaded', () => {

});

const searchMovies = async () => {
    const textSearch = document.getElementById('search-movie').value;
    if(textSearch.length < 3) { return; }
    const movies = await getMovie(textSearch);
    console.log(movies);

    let html = "";

    movies.forEach(movie => {
        const { id, poster_path, title, overview} = movie;
        const urlMovie = `../movie.html?id=${id}`;
        const movieCover = `https://image.tmdb.org/t/p/w500${poster_path}`;
        html += `
            
                <div class="col-xs-12 col-sm-6 col-md-3 col-custom"">
                    <a href="${urlMovie}" class="card custom-card">
                        <img class="card-img-top" src="${movieCover}" alt="${title}">
                        <div class="card-body">
                            <h4 class="card-title text-cente m-0">${title}</h4>
                        </div>
                    </a>
                </div>
            
        `;
    });

    document.getElementsByClassName('list-cards')[0].innerHTML = html;
}

const getMovie = (textSearch) => {
    const url = `${URL_PATH}/3/search/movie?api_key=${API_KEY}&language=es-ES&page=1&query=${textSearch}&page=1&include_adult=true`;

    return fetch(url)
        .then(response => response.json())
        .then(result => result.results)
        .catch(error => console.log(error));
}