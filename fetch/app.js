function getPopularMovie () {
    const url = "https://api.themoviedb.org/3/movie/popular?api_key=9f256582b4040e6fc78b7a6ca742ccb7&language=ES-es&page=1";

    fetch(url).then(response => {
        console.log(response);
        return response.json();
    }).then(results => {
        console.log(results)

        const peliculas = results.results;

        peliculas.forEach(pelicula => {
            const {title, vote_count, poster_path} = pelicula;
            console.log(title);
        });
    });
}

getPopularMovie();