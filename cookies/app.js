const crearCookie = (clave, valor) => {
    const valorEscape = escape(valor);
    let mesSiguiente = new Date();

    mesSiguiente.setMonth(mesSiguiente.getMonth() + 1);

    console.log(mesSiguiente);

    document.cookie = `${clave}=${valorEscape};expires=${mesSiguiente};`;
};

const borrarCookie = clave => {
    let mesAnterior = new Date();
    mesAnterior.setMonth(mesAnterior.getMonth() - 1);
    console.log(mesAnterior);
    document.cookie = `${clave}=Y;expires=${mesAnterior};`;
}

const obtenerCookies = () => {
    const cookies = document.cookie;
    const arrayCookies = cookies.split("; ");
    let objetoCookies = {};

    arrayCookies.forEach(cookie => {
        const cookieArray = cookie.split("=");
        console.log(cookieArray);
        const keyItem = cookieArray[0];
        objetoCookies[keyItem] = unescape(cookieArray[1]);
    });
    console.log(objetoCookies);
    return objetoCookies;
}


crearCookie("marcaCoche", "au;di");
crearCookie("potenciaCoche", "180cv");

//borrarCookie("marcaCoche");

const cookies = obtenerCookies();
console.log('Valor metodo', cookies);